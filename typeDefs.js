module.exports = `
    type Product {
        id: ID!
        name: String!
        slug: String!
        description: String!
        sku: String!
        status: String!
    }

    type Brand {
        id: ID!
        name: String!
        slug: String!
        description: String!
    }

    type Query {
        allProducts: [Product]
        Product(id: ID!): Product
        allBrands: [Brand]
        Brand(id: ID!): Brand
    }
`